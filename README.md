# Setup

## Debian
```
git clone https://gitlab.com/termuxrep/repo-arm-selfbuild
cd repo-arm-selfbuild
./scripts/setup-ubuntu.sh
termux-create-package <yourpackage>/<yourpackage>.yml
```
If you want Install the Package on Debian then make this step after the operation
```
dpkg-deb -R <yourpackage>*.deb tmp
cd tmp
mkdir -p usr
cp -rf data/data/com.termux/files/usr/* usr
```
or change in the .yml
```
from
installation_prefix: /data/data/com.termux/files/usr
to
installation_prefix: /usr
```
## Arch
```
git clone https://gitlab.com/termuxrep/repo-arm-se
lfbuild
cd repo-arm-selfbuild
./scripts/setup-arch.sh
termux-create-package <yourpackage>/<yourpackage>.
yml
or
cd <yourpackage>
makepkg -si
``` 
For Arch user, if you manipulate the PKGBUILD can you install the termux-package on your device

## Termux
```
git clone https://gitlab.com/termuxrep/repo-arm-se
lfbuild
cd repo-arm-selfbuild
./scripts/setup-termux.sh
```
The Script will use automatically your package manager
