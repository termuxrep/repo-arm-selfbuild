#!/bin/bash

if command -v sudo &> /dev/null; then
	update="sudo apt update"
	upgrade="sudo apt upgrade -y"
	install="sudo apt install -y"
else
	update="apt update"
	upgrade="apt upgrade"
	install="apt install -y"
fi

$update
$upgrade
$install python3
$install python-pip
$install python3-virtualenv
python3 venv -m build-packages
source build-packages/bin/activate
pip install ruaml.yaml
pip install termux-create-package

