#!/bin/bash

if command -v sudo &> /dev/null; then
	update="sudo pacman -Syy"
	upgrade="sudo pacman -Syu --noconfirm"
	install="sudo pacman -S --noconfirm"
else
	update="pacman -Syy"
	upgrade="pacman -Syu --noconfirm"
	install="pacman -S --noconfirm"
fi

$update
$upgrade
$install python
$install python-pip
$install python3-virtualenv
python3 venv -m build-packages
source build-packages/bin/activate
pip install ruaml.yaml
pip install termux-create-package

