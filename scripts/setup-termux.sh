#!/bin/bash

# Check Manager
if command -v apt &> /dev/null; then
        install="apt install -y"
        search="apt search"
        remove="apt remove -y"
	upgrade="apt update && apt upgrade -y"
	$upgrade
	$install build-essential
	$install binutils
elif command -v pacman $> /dev/null; then
        install="pacman --noconfirm -S"
        search="pacman -Ss"
        remove="pacman -R --noconfirm"
	upgrade="pacman -Syu --noconfirm"
	$upgrade
	$install base-devel
	$install python3-pip
else
        echo "Your Device is not supported"
        echo "You need APT or Pacman for this action"
fi

$install python3
$install termux-create-package
$install yq
pip install ruamel.yaml==0.17.21
